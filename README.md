# Customer service

Application is for simple customer service. Users can ask questions and 
administrators answer those questions. One question can have more answers, 
something like chat between user and admin.

Live website: http://customer-service.ivan-matozan.from.hr/

.htaccess: test/1234

#### Technologies used:
* PHP
* MySQL
* Slim Framework
* Eloquent ORM
* Twig template engine
* Bootstrap frontend framework